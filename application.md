Gyula: Na Matyi, gyere, számoljuk el a múlthetet. Te még új vagy, segítek
neked. A lényeg, hogy ha ketten voltunk, akkor mind a kettőnkenk ugyanazt kell
írnunk.

Matyi: Jó. Nekem az első napra 6 óra jött ki.

Gyula: Nekem 12. Te hogy számoltad?

Matyi: Hát, hogy mire kiértünk a teszt pályára, 10 óra lett. Ott voltunk 4-ig,
az összesen 6 óra. Gondolom utána a driftelést meg, hogy elmentünk a Lidl-be,
azt nem kell elszámolni. Ugye?

Gyula: Hülye vagy. Reggel a hotelben találkoztunk. Ott megkérdeztem tőled,
hogy szerinted kell-e tankolnunk. Ez már gyakorlatilag munka! Én innen
számolom. Szóval. Reggel nyolckor kezdtük a melót.

Matyi: A reggelit is munkának számítod?

Gyula: Miről beszéltünk a reggeli közben.

Matyi: Hát... az autókról.

Gyula: Tehát szakmáztunk. Ez munka. Nem kérdés. Én azt is elszámolom, ha a
totalcar-t olvasom, vagy autós képeket nézek a neten. Ez alap a csoportban.
Mit gondolsz, Jani szabadidejében tunningolja az otthoni verdát? Az önképzés
mind bele van építve az elszámolásokba.

...

A: Kijössz velem ma Lacházára az Alfával, hogy legyen target objektem az ACC-hez?

B: Hát, az Alfával nehezen...

A: Hogyhogy? Már megint szervizben van?

B: Nem, csak hát a hátsó kerekeit egy kicsit eljátszottuk...

...

Gyula: Figyelj már, volt már neked olyan, hogy nem akart betölteni a doors?

Matyi: Nem tudom... Mi az a doors?

...

Hú, gyere csak ide Tibi, te már régóta vagy itt. Hogy szoktátok az ACC-s
drive-off-ot tesztelni?

Ja, én még olyat nem teszteltem. Na csumi!

...

Matyi: Tehát akkor minden nap 12 órát dolgoztunk. Mi van a hétvégével? Szombat
éjszaka érkeztünk meg éjfél után.

Gyula: Ez nagyon egyszerű. Én miután hazaértem, még megnéztem egy pornót, amiben elhasználtak négy gumit. Ezek gyakorlatilag kollégák! Így én ezt is hozzácsapom az elszámoláshoz. Tehát így az jön ki, hogy vasárnap is dolgoztunk.

Matyi: És mi van a pénteki drift versennyel, amit Lidl parkolójában rendeztünk,
mert _véletlenül_ kiömlött a tej az útra? Ott 3 órát elbohóckodtunk. Ha reggelitől számoljuk, akkor az már 14 óra, amiből kettőt nem fizetnek ki. Akkor azt bent hagyjuk?

Gyula: Megőrültél. Figyelj! Órát nem dobunk ki, csak vasárnapra tesszük.
Itt van két óra, ott meg három, az összesen hat. Hat meg hat az 36, innen
lecsípünk, ott szétkenjük. Az annyi mint 12 óra vasárnap. Érted? 

Matyi: Nem, de elhiszem, ha te mondod.

...

A: Sziasztok! Megy valaki maLacházára?

B: Attól függ. Van kocs maLacházán? Ha igen, én megyek.

A: Na de most komolyan.

B: Pisti most jön haza. Tudod, Bécs, Pannonhal, MaLacháza. Ő tuti ott lesz.

A: Jó, csak azért kérdem mert ott maradt egy al maLacházán. Nem tudom, hogy elmenjek-e érte ma Lacházára vagy ne. Nem biztos, hogy kitart a gumi.
Tudod ezen a héten is megy az Alfa vándorútrát játszunk.

B: Az meg mi?

A: Hát, hogy mindenki egy napig használja az autót, csikorgatva, ahogy kell.
Vesszük is kamerával, nehogy valaki csaljon. És akinél kilyukad a gumi, az vesztett.

...

A: Akkor lefoglalom a jövő hetet. Ez a hotel jó lesz?

B: Hülye vagy? Nézd csak itt egy kicsit drágábban van jakuzzi is. Ott már ismernek minket név szerint. Oda menjünk.

A: Akkor pénteken jövünk haza?
B: Dehogy is, szombaton.

...

A: Hallod? Mennyi ideig tart a Stop and Go-t letesztelni?

B: Amikor mi csináltuk, két embernek volt egy nap. De meg tudja egy ember is csinálni fél nap alatt, ha kell. Úgy mentünk ki, hogy ne maradjunk le a dugóról, és természetesen délben itt kajáltunk. Aztán délután megint ki.

...

A: Hazahoztuk az új autót, állítólag nem lehet 5l felé vinni a fogyasztását, annyira
takarékos. Mit gondoltok mennyit fogyasztottam százon, amikor hazahoztuk?

B: 6-ot? 6,5-et?

A: 11,5-öt! Nem mondom, csak ritkán mentünk le 200 alá, de akkor is csak úgy, hogy
nem tartottunk 5 méternél nagyobb követési távolságot.